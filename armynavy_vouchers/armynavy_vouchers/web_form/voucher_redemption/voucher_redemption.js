frappe.ready(function () {
	const vouchers = S.data([]);
	const originalSave = frappe.web_form.save;

	frappe.web_form.save = function (...args) {
		if (!vouchers().length) {
			frappe.throw(__("Please apply at least one voucher before proceeding"), __("Please Apply a Voucher"));
			return;
		}

		originalSave.call(frappe.web_form, ...args);
	};

	frappe.web_form.handle_success = function (data) {
		window.location.href = '/vouchers/confirm/' + data.name;
	};

	frappe.web_form.after_load = function () {
		const isLoading = S.data(false);
		const voucherInput = S.data('');
		const $redeemInput = $('#redeemInput');
		const $redeemButton = $('#redeemButton');
		const $vouchersContainer = $('#vouchersContainer');

		S(function () {
			frappe.web_form.set_value('vouchers_input', vouchers().map((voucher) => voucher.voucher_code).join(','));
		});

		S(function () {
			$redeemInput.attr('disabled', isLoading());

			if (isLoading()) {
				$redeemButton.html(`
					<div class="spinner-border spinner-border-sm" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				`);
			} else {
				$redeemButton.text('Redeem');
			}
		});

		S(function () {
			$redeemButton.attr('disabled', isLoading() || !voucherInput());
		});

		S(function () {
			$redeemInput.val(voucherInput());
		});

		S(function () {
			$vouchersContainer.html(
				vouchers().map(function(voucher) {
					return `
						<div class="card mb-4">
							<div class="card-body">
								<div class="container">
									<div class="row">
										<div class="col-sm-12 col-md-4 mb-4">
											<img src="${voucher.bundle.bundle_image}" />
										</div>
										<div class="col-sm-12 col-md-8">
											<h5 class="card-title">${voucher.bundle.bundle_name} (${voucher.voucher_code})</h5>
											<h6 class="card-subtitle">
												<s>&#8369;${voucher.bundle.original_price}</s> &#8369;${voucher.bundle.sale_price}
											</h6>
											<p class="card-text">
												${voucher.bundle.bundle_description}
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					`;
				}).join('\n')
			);
		});

		$redeemInput.on('keyup', function () {
			voucherInput($redeemInput.val());
		});

		$redeemButton.click(function () {
			isLoading(true);

			if (!vouchers().find((voucher) => voucher.voucher_code === voucherInput())) {
				frappe.call({
					method: 'armynavy_vouchers.armynavy_vouchers.redeem_voucher',
					args: {
						voucher: voucherInput(),
					},
					callback: function (r) {
						if (!r.exc) {
							const response = r.message;

							if (response.message) {
								frappe.msgprint(response.message);
							} else {
								voucherInput('');
								vouchers(vouchers().concat(response));
							}
						}

						isLoading(false);
					}
				});
			} else {
				isLoading(false);
				frappe.msgprint("You've already claimed that voucher");
			}
		});
	};
});

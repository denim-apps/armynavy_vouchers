import frappe

@frappe.whitelist(allow_guest=True)
def redeem_voucher(voucher, ignore_claims = False):
  voucherRecords = frappe.db.get_all(doctype='Voucher', filters={
    'voucher_code': voucher
  }, page_length=1)

  if len(voucherRecords):
    voucherRecord = frappe.get_doc('Voucher', voucherRecords[0].name)

    if ignore_claims or not voucherRecord.claimed:
      if voucherRecord.bundle:
        bundle = frappe.get_doc('Voucher Bundle', voucherRecord.bundle)
        voucherRecord.bundle = bundle

      return voucherRecord

  return { 'message': 'Voucher not found' }

@frappe.whitelist(allow_guest=True)
def process(action, order):
  # Retrieve the order
  order = frappe.get_doc('Order', order)

  if order:
    if action == 'confirm':
      # Validate the vouchers haven't been claimed before
      for order_voucher in order.vouchers:
        voucher = frappe.get_doc('Voucher', order_voucher.voucher)

        if voucher.claimed:
          return { 'message': 'Voucher ' + order_voucher.voucher + ' has already been redeemed' }

      # Mark the order as confirmed
      order.status = 'Confirmed'
      order.save(True)
      return { }

    if action == 'cancel':
      # Mark the order as cancelled
      order.status = 'Cancelled'
      order.save(True)
      return { }

  return { 'message': 'An error occurred' }

@frappe.whitelist(allow_guest=False)
def get_voucher_details(name):
  # Retrieve the order
  order = frappe.get_doc('Order', name)
  vouchers = []

  for voucher in order.vouchers:
    vouchers.append(redeem_voucher(voucher.voucher, True))

  return vouchers

// Copyright (c) 2021, JC Gurango and contributors
// For license information, please see license.txt

frappe.ui.form.on('Order', {
	refresh: function (frm) {
		document.getElementById('claimedVouchers').innerHTML = `<div class="spinner-border"></div>`;

		frappe.call({
			method: 'armynavy_vouchers.armynavy_vouchers.get_voucher_details',
			args: {
				name: frm.doc.name,
			},
			callback(r) {
				if (r.message) {
					const expanded = r.message;

					document.getElementById('claimedVouchers').innerHTML = expanded.map(function (voucher) {
						return `
						<div class="card mb-4">
							<div class="card-body">
								<div class="container">
									<div class="row">
										<div class="col-sm-12 col-md-4 mb-4">
											<img src="${voucher.bundle.bundle_image}" />
										</div>
										<div class="col-sm-12 col-md-8">
											<h5 class="card-title"><a href="/app/voucher/${voucher.voucher_code}">${voucher.bundle.bundle_name} (${voucher.voucher_code})</a></h5>
											<h6 class="card-subtitle">
												<s>&#8369;${voucher.bundle.original_price}</s> &#8369;${voucher.bundle.sale_price}
											</h6>
											<p class="card-text">
												${voucher.bundle.bundle_description}
											</p>
											<p>
												<label><input type="checkbox" ${voucher.claimed ? 'checked' : ''} disabled /> Claimed</label><br />
												<label><input type="checkbox" ${voucher.redeemed ? 'checked' : ''} disabled /> Redeemed (in Seller Portal)</label>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						`;
					}).join('\n');
				}
			}
		});
	}
});

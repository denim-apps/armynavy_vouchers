# Copyright (c) 2021, JC Gurango and contributors
# For license information, please see license.txt

import frappe
import json
from frappe.model.document import Document

class Order(Document):
	def insert(self, ignore_permissions=True, ignore_links=True, ignore_if_duplicate=True, ignore_mandatory=True):
		if self.vouchers_input:
			voucher_inputs = self.vouchers_input.split(',')

			for input in voucher_inputs:
				voucher = frappe.get_doc('Voucher', input)
				
				if voucher and not voucher.claimed:
					orderVoucher = frappe.new_doc('Order Voucher')
					orderVoucher.voucher = voucher.name
					self.append('vouchers', orderVoucher)

		Document.insert(self, ignore_permissions, ignore_links, ignore_if_duplicate, ignore_mandatory)
	def save(self, ignore_permissions=True, ignore_version=True):
		if self.status == 'Confirmed' or self.status == 'Completed':
			# Update the vouchers
			for order_voucher in self.vouchers:
				voucher = frappe.get_doc('Voucher', order_voucher.voucher)

				if self.status == 'Confirmed':
					voucher.claimed = True
					
				if self.status == 'Completed':
					voucher.redeemed = True

				voucher.save(True)

		Document.save(self, ignore_permissions, ignore_version)

from frappe import _

def get_data():
	return [
		{
			"module_name": "Armynavy Vouchers",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Armynavy Vouchers")
		}
	]

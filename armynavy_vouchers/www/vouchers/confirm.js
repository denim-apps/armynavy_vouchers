frappe.ready(function () {
  const loading = S.data(false);
  const action = S.data('');
  const $confirm = $('#confirm');
  const $cancel = $('#cancel');

  S(function() {
    $confirm.attr('disabled', loading()).find('.spinner-border').addClass('hide');
    $cancel.attr('disabled', loading()).find('.spinner-border').addClass('hide');

    if (loading()) {
      if (action() === 'confirm') {
        $confirm.find('.spinner-border').removeClass('hide');
      }

      if (action() === 'cancel') {
        $cancel.find('.spinner-border').removeClass('hide');
      }
    }
  });

  const run = function() {
    loading(true);

    frappe.call({
      method: 'armynavy_vouchers.armynavy_vouchers.process',
      args: {
        action: action(),
        order: orderId,
      },
      callback: function (r) {
        if (!r.exc) {
          const response = r.message;

          if (response.message) {
            frappe.msgprint(response.message);
          } else {
            if (action() === 'confirm') {
              window.location.href = '/vouchers/confirmed';
            } else {
              window.location.href = '/vouchers';
            }
          }
        }

        loading(false);
      }
    });
  };

  $confirm.click(function() {
    action('confirm');
    run();
  });

  $cancel.click(function() {
    action('cancel');
    run();
  });
});

import frappe

def get_context(context):
  order = frappe.get_doc('Order', frappe.form_dict.order)

  if order.status != 'Submitted':
    raise Exception('')

  for order_voucher in order.vouchers:
    order_voucher.voucher = frappe.get_doc('Voucher', order_voucher.voucher)
    order_voucher.voucher.bundle = frappe.get_doc('Voucher Bundle', order_voucher.voucher.bundle)

  context.order = order
  context.hide_login = True
  context.banner_image = '/assets/armynavy_vouchers/images/an-icon.png'

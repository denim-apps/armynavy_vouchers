import frappe

def get_context(context):
  settings = frappe.get_doc('ArmyNavy Settings')

  return { 'hide_login': True, 'banner_image': '/assets/armynavy_vouchers/images/an-icon.png', 'content': settings.redemption_page_content }
